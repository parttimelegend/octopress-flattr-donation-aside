Octopress Flattr Donation Aside
================================

This is a new custom aside for [Octopress](https://github.com/imathis/octopress?ref=commandbarr) that shows a button for [Flattr](http://www.flattr.com) in your sidebar.

To install copy the files included to your octopress installation.

Obviously change the User Id to your own, or keep mine. I don't mind. ;)

Then change the line:

`default_asides: [`

Add to it:

`custom/asides/flattr.html`

[![Flattr this git repo](http://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=parttimelegend&url=https://github.com/PartTimeLegend/octopress-flattr-donation-aside&title=https://github.com/PartTimeLegend/octopress-flattr-donation-aside&language=&tags=github&category=software) 
